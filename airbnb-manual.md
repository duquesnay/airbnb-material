1) english version
2) version française

# ENGLISH
Here’s the answers to usual questions that I have been asked by guest. Feel free to ask me for more.


## Lights
The entrance light button is close to the entry door. 
Kitchen and Living room buttons are on the side of the refrigerator, entering the kitchen.
The others lights are classic.


## Internet
See airbnb site for wifi and password.
This is fiber internet, super fast, unrestricted usage. The wifi is available also in the bedroom but you may experience a weaker signal dependeing on your device, since the walls are thick.

## Cooking with induction
A few rules
- dont scratch it to clean it : no scratching wiper, no stratching detergent like CIF, prefer vitroclean or simply hot water
- clean it with the violet microfiber tissue if you want a super neat result
- only use pots and pans for induction, but you should find everything you need there : pots, pans, even an induction wok. They are stored on the left of the induction.
To turn it on : there’s an on/off button, then you set the level of power you want on each fire. 
This appliance « beep » often : this happens when you put something on the command buttons by mistake. The appliance is picky about this, even a tissue can make a bip.

## The oven
Cooking is classic with good options (rotating heat, roasting, controlled temperature, programming, etc)
Cleaning it : never clean it with detergent of any kind not any scratching sponge. 
It self cleans when you start the pyrolyse mode (do this when you are not here). You better open the big window and leave the place, leave the hood working, since it can be smelly.
It will burn everything at hot temperature in it for 2:30 hours, then you will have only dust remaining, wipe it with a humid sponge only. 
For more details, depending on the dirtiness, google instructions on using pyrolyse cleaning.
 
## Dish washer
Don’t use 3in1 detergent : some yellow doses are available under the kitchen sink, in a red metal box.
Starting it is easy : turn it on, let it on automatic program or choose manually, then start it sith the start button. It will start when you close it. It’s a silent dish washer, you will see on the floor a blue light signaling when it works. Then there will be 4 bips whe washing is over.

## The hood
It gives a great soft light on the bar if you want to have a drink. I usually uses this instead of the kitchen light.
- button 5/right : light on/off
- button 3/central : hood on/off
- button 4/right of the center : lower venting
- button 2/left of the center : higher venting
- button 1/left : booster mode (active for 10 minutes)

## Bathroom tap
open it slowly or the flow will be so strong that you may get splashed. Sorry, it seems this kind of tap wasn’t designed for this kind of sink :)

## The shower
- left handle
-- up = shower
-- down = bathtub tap
- right handle
-- up = colder
-- down = warmer
- Square handle on the shower tube, mid height
-- left/reverse clock rotate = mode shower
-- right/clock rotate = mode small jet

## Washer Dryer
Its a top notch washer dryer. It relatively silent, but please don’t use it at night, as it will resonate at the neighbours’ down level.
The eco mode is very efficient, designed to wash clothes without sorting them by type of tissue. 
Other programs is classic.
Blue detergent individual dose are available under the sink

## Vapor ironing appliance
its usually in the living room close to the bay window or in the entrance shelves.
- don’t use descaling agent. The machin is self descaling. In theory the cleaning service does the maintenace, but the first time you use it you may want to check on a wiping tissue that there is no scale.

## Food on the shelves
Use it as you like, leave anything you like.

## Charging mobile devices
Some powerful USB charger are at your disposal beside the couch and beside the bed, so you don’t need chargers. You will find basic adapteurs (various iPhones, micro-usb), but if you need a special cable you better bring one.

## Music speakers
All music appliances are bluetooth. You will find always 
- an ON/OFF button, usually discreet, to press 3 to 5 seconds usually 
- a pairing button to allow pairing with your phone
- you will see it available for pairing on your mobile / computer

On the living room, the Speaker is HK Aura, its the bubble close to the TV. It is also available via Airplay for Apple devices. It is almost always on.
On the bedroom, the speaker is JBL Charge 2. It is a cylinder on the bed table. You can charge it with a micro-USB power outlet (just beside the bed).
In the shower, you can use the small speaker that remains with a suction pad. Press 3 seconds the ON button to light it on. It is calledpaired under the name Avanca WPS. Remember to shut it off after your shower because it does do by itself. You can charge it with the USB power outlet of the couch.

## Using the sleeping couch
pillows and blanket are stored on the left part of the couch, which serves as a trunk (catch the most outside border and pull it up). Leave the blanket in the living room when you leave if you use it.
To unfold the bed, a brown handle is under the border of the main seat. Simply pull it up, then toward you. No strength is required, it should follow a « natural » movement. Unfold the last part (you may have to force it a little bit this time).
Reverse the steps to fold the couch again.

# FRANCAIS
Voici une petite compilation des questions qu’on m’a posées pour utiliser l’appartement. N’hésitez pas à me demander si vous avez d’autres questions

## Allumer les lumières 
Le bouton de la lumière de l’entrée est celui près de la porte (pas toujours pratique, des travaux sont prévus) .
Les interrupteurs de la cuisine et du grand lampadaire du salon se trouvent sur le côté du réfrigérateur. Les autres interrupteurs sont sur les lampadaires et lampes eux-mêmes.

## Internet 
C’est de la fibre, ça dépote, c’est en illimité. Usage non restreint. Le wifi capte aussi dans la chambre mais le signal est peut-être plus faible car la chambre est entourée par des murs porteurs. 

## Cuisiner avec la plaque à induction 
Quelques règles de base : 
- ne pas la grater pour la nettoyer: pas d’éponge qui grate, pas de produit type Cif, préférer du vitroclean ou tout simplement de l’eau brûlante
- essuyer avec le chiffon microfibre violet si vous voulez un résultat super clean 
- n’utiliser que des ustensiles pour plaque à induction. Normalement tout est fourni : plusieurs poêles, casseroles et wok
- pas de métal dans les poêles anti-adhésives. Ni fourchette, ni cuillère. Normalement, tous les ustensiles nécessaires sont disponibles.

Pour allumer la plaque, on touche l’interrupteur on/off, puis on règle les plaques. Il y a aussi un minuteur. Les casseroles et les poêles sont dans les tiroirs côtés gauche, elles sont toutes compatibles induction.

Pas de panique si la plaque émet des bip : ça arrive dès qu’on pose quelque chose par erreur sur les boutons de commande. Elle est un peu tatillonne sur ce point, même un torchon déclenche l’alerte. Pour le reste, elle est très serviable.

## Le four 
Ne jamais le laver avec du détergent. C’est un four à pyrolyse, on la déclenche (je vous suggère de laisser ça à la femme de ménage), puis on nettoie la cendre restante avec une éponge. 
Pour le reste, c’est un fonctionnement classique.

## Le lave-vaisselle 
Ne pas utiliser de produit 3-en-1 : des pastilles jaunes se trouvent dans une boîte en métal rouge sous l’évier de la cuisine 
Pour le reste, il suffit de le programmer (ou de le laisser en programme automatique), de le déclencher avec le bouton on et le fermer. C’est un lave vaisselle silencieux, on entend parfois le léger bruit des jets d’eau, et pour vérifier qu’il marche regardez s’il y a une lumière bleue au sol. Il émettra 4 bips lorsque le lavage est terminé.

## La hotte 
La hotte donne une très bonne lumière tamisée en soirée. Je préfère souvent sa lumière à celle de la cuisine. 
- bouton 5/droite : lumière on/off 
- bouton 3/central : hotte on/off 
- bouton 4/droite du centre : - fort
- bouton 2/gauche du centre : + fort 
- bouton 1/ gauche : mode booster (activé pour quelques minutes) 

## Lavabo de la salle de bain 
Ouvrez très doucement le robinet du lavabo, sinon le jet est tellement fort que vous serez aspergé. Désolé, ce type de robinet n’est pas du tout adapté à ce type de vasque.

## La douche 
vous avez normalement à disposition un jeu de serviettes par personne. Si vous souhaitez en utiliser d’autres, merci de les laver/sécher avant votre départ (cf sèche linge), car le lavage des serviettes et draps n’est prévu que pour le nombre de visiteurs.
- la manette de gauche 
-- vers le haut = faire couler la douche 
-- vers le bas = faire couler le robinet de la baignoire 
- la manette de droite 
-- vers le haut = plus froid 
-- vers le bas = plus chaud 
- la manette carré sur le montant de la douche 
-- vers la gauche / sens inverse d’une montre = vers la tête de douche en haut 
-- vers la droite / sens des aiguilles d’une montre = vers le petit jet

## Lave-linge 
C’est un lave linge séchant, il est top. Il est relativement silencieux mais interdiction de l’utiliser la nuit, car la résonnance dérange les voisins du dessous. 
Le mode éco est un lavage à froid très efficace. Il est conçu pour mettre tout type de vêtement sans s’embêter à trier :) 
Le reste des programmes est plutôt intuitif, sinon… le manuel doit être dispo sur Internet :). 
Des pastilles bleues sont dans le tiroir du bas à gauche de la machine.

## Station vapeur pour repasser
la station vapeur est dans le salon près de la baie vitrée, la planche est dans le placard. 
- ne pas utiliser de détartrant. La machine gère le détartrage elle-même. En théorie la femme de ménage fait la maintenance, vérifier tout de même auparavant sur un torchon qu’il n’y a pas de traces de tartre.

## Nourriture dans les placards 
Servez-vous! 
Laissez quelque chose si vous le souhaitez, ça servira sûrement à quelqu’un

## Charger vos appareils mobiles
Des chargeurs USB très puissants sont disponibles sur le canapé et près du lit. A peu près tous les adaptateurs sont fournis, mais si vous avez un câble spécial amenez-le.

## Utiliser les enceintes
Toutes les enceintes sont en bluetooth. Généralement il y a un bouton ON/OFF à trouver, souvent discret, à appuyer 3 secondes, puis un bouton pour autoriser l’appairage, à appuyer 3 à 5 secondes aussi. Ensuite vous verrez ces enceintes disponibles comme nouvel appareil bluetooth pour votre mobile / votre ordinateur.
Dans le salon, l’enceinte est visible en bluetooth en tant que HK Aura. C’est la bulle posée à côté de la télé. Cette enceinte est aussi accessible en AirPlay pour les appareils Apple. Elle reste toujours alimentée. 
Dans la chambre, elle est visible sous le nom de JBL Charge 2. C’est le cylindre sur la tablette du lit. Elle peut être chargée en micro-USB (chargeur dispo juste à côté).
Dans la douche, une petite enceinte ventouse peut être allumée (appuyer 3 secondes sur le bouton on). Elle est visible en tant que Avanca WPS. Pensez à l’éteindre ensuite. Elle peut être chargée avec le chargeur USB du canapé.

## Utiliser le canapé-lit
La couette et les oreillers sont disponibles dans la banquette sur le côté, qui peut se soulever (attraper l’extrémité extérieur et soulever). Laissez-la dans le salon à votre départ.
Pour déplier le canapé, une languette marron dépasse en dessous des coussins rouges sur le bord vers vous. Soulever simplement, puis amener vers vous. Cela ne nécessite pas d’effort, il faut suivre le mouvement « naturel ». Ensuite dépliez la dernière partie (il faut forcer un peu à cette étape).
Procédez en sens inverse pour replier le canapé.